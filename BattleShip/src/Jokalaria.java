
public class Jokalaria {
	//atributuak
	private ListaOntzia listaOntzia;
	private Tableroa tableroa;

	//Eraikitzailea(k)
	public Jokalaria(){
		listaOntzia=new ListaOntzia();
		tableroa= new Tableroa();
	}

	public void sartuOntzia(int x,int y,Ontzia pOntzia){
		tableroa.ontziaSartu(x, y, pOntzia);
	}
	public void sartuListan(Ontzia pOntzia){
		listaOntzia.addOntzia(pOntzia);
		listaOntzia.inprimatu();
	}
	
}
