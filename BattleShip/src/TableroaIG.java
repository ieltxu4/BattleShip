import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;

public class TableroaIG extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private static TableroaIG nireTableroaIG=null;
	private JPanel contentPane;
	private String letrak="ABCDEFGHIJ";
	private KasillaIG[][] tableroa;
	private KasillaIG[][] tableroa2;
	private int kasillakop=0;
	private int fragatKop=4;
	private int hegazkinKop=1;
	private int suntsiKop=3;
	private int itsasKop=2;
	private int egoera=0;
	private int unekoOntzia=0;
	JButton itsaspeko;
	JButton suntsitzailea;
	JButton fragata;
	JButton hegazkina;
	JPanel zureArmak;
	JPanel denda;
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TableroaIG.getTableroaIG().setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	private TableroaIG() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
	
		JPanel goian = new JPanel();
		contentPane.add(goian, BorderLayout.NORTH);
		goian.setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel zureTableroa = new JPanel();
		goian.add(zureTableroa);
		zureTableroa.setBorder(new TitledBorder("Zure Tableroa"));
		
		JPanel etsaiTableroa = new JPanel();
		goian.add(etsaiTableroa);
		etsaiTableroa.setBorder(new TitledBorder("Etsaiaren Tableroa"));
		
		JPanel behean = new JPanel();
		contentPane.add(behean, BorderLayout.SOUTH);
		behean.setLayout(new GridLayout(1, 0, 0, 0));
		
		zureArmak = new JPanel();
		behean.add(zureArmak);
		zureArmak.setBorder(new TitledBorder("Zure Armak"));
		
		
		
		denda = new JPanel();
		behean.add(denda);
		denda.setBorder(new TitledBorder("Denda"));
		
		JPanel erdikoa = new JPanel();
		contentPane.add(erdikoa, BorderLayout.CENTER);
		erdikoa.setBorder(new TitledBorder("Ontziak"));
		
		JRadioButton rdbtnGora = new JRadioButton("Gora");
		erdikoa.add(rdbtnGora);
		rdbtnGora.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				TableroaIG.nireTableroaIG.aldatuEgoera(0);
			}
		});
		
		JRadioButton rdbtnBehera = new JRadioButton("Behera");
		erdikoa.add(rdbtnBehera);
		rdbtnBehera.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				TableroaIG.nireTableroaIG.aldatuEgoera(1);
			}
		});
		
		JRadioButton rdbtnEskuinera = new JRadioButton("Eskuinera");
		erdikoa.add(rdbtnEskuinera);
		rdbtnEskuinera.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				TableroaIG.nireTableroaIG.aldatuEgoera(2);
			}
		});
		
		JRadioButton rdbtnEzkerrera = new JRadioButton("Ezkerrera");
		erdikoa.add(rdbtnEzkerrera);
		rdbtnEzkerrera.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				TableroaIG.nireTableroaIG.aldatuEgoera(3);
			}
		});
		hegazkina = new JButton("Hegazkina x"+this.hegazkinKop);
		erdikoa.add(hegazkina);
		hegazkina.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				TableroaIG.nireTableroaIG.aldatuKasillaKop(4);
				hegazkina.setText("Hegazkina x"+TableroaIG.nireTableroaIG.getHegazkin());
				
				
			}
		});
		fragata = new JButton("Fragata x"+this.fragatKop);
		erdikoa.add(fragata);
		fragata.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				TableroaIG.nireTableroaIG.aldatuKasillaKop(1);
				fragata.setText("Fragata x"+TableroaIG.nireTableroaIG.getFragata());
				
			}
		});
		
		suntsitzailea = new JButton("Suntsitzailea x"+this.suntsiKop);
		erdikoa.add(suntsitzailea);
		suntsitzailea.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				TableroaIG.nireTableroaIG.aldatuKasillaKop(2);
				suntsitzailea.setText("Suntsitzailea x"+TableroaIG.nireTableroaIG.getSuntsitzaile());
				
			}
		});
		
		itsaspeko = new JButton("Itsaspekoa x"+this.itsasKop);
		erdikoa.add(itsaspeko);
		itsaspeko.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				TableroaIG.nireTableroaIG.aldatuKasillaKop(3);
				itsaspeko.setText("Itsaspeko x"+TableroaIG.nireTableroaIG.getItsaspeko());
				
				
			}
		});
		
		zureTableroa.setLayout(new GridLayout(11, 11));
		etsaiTableroa.setLayout(new GridLayout(11, 11));
		zureTableroa.add(new JLabel());
		etsaiTableroa.add(new JLabel());
		tableroa=new KasillaIG[10][10];
		tableroa2=new KasillaIG[10][10];
		for(int i=0;i<10;i++){
			zureTableroa.add(new JLabel(letrak.substring(i, i+1),SwingConstants.CENTER));
			etsaiTableroa.add(new JLabel(letrak.substring(i, i+1),SwingConstants.CENTER));
		}
		int zutabe;
		int errenkada;
		for(errenkada=0;errenkada<tableroa.length;errenkada++){
			for(zutabe=0;zutabe<tableroa[errenkada].length;zutabe++){
				if(zutabe==0){
					zureTableroa.add(zureTableroa.add(new JLabel(""+(errenkada+1),SwingConstants.CENTER)));
					etsaiTableroa.add(etsaiTableroa.add(new JLabel(""+(errenkada+1),SwingConstants.CENTER)));
				}
				KasillaIG ka=new KasillaIG(errenkada, zutabe);
				KasillaIG ka1=new KasillaIG(errenkada, zutabe);
				ka.setBackground(Color.GRAY);
				ka1.setBackground(Color.GRAY);
				tableroa[errenkada][zutabe]=ka;
				tableroa2[errenkada][zutabe]=ka1;
				zureTableroa.add(tableroa[errenkada][zutabe]);
				etsaiTableroa.add(tableroa2[errenkada][zutabe]);

			}
		}
		
	}
	public static TableroaIG getTableroaIG(){
		if(nireTableroaIG==null)
			nireTableroaIG=new TableroaIG();
		return nireTableroaIG;
	}
	public void kenduKasilla(){
		this.kasillakop--;
	}
	public int getHegazkin(){
		return this.hegazkinKop;
	}
	public int getItsaspeko(){
		return this.itsasKop;
	}
	public int getFragata(){
		return this.fragatKop;
	}
	public int getSuntsitzaile(){
		return this.suntsiKop;
	}
	public void kenduHegazkin(){
		this.hegazkinKop--;
	}
	public void kenduItsaspeko(){
		this.itsasKop--;
	}
	public void aldatuunekoOntzia(int i){
		this.unekoOntzia=i;
	}
	public void kenduFragata(){
		this.fragatKop--;
	}
	public void kenduSuntsitzaile(){
		this.suntsiKop--;
	}
	public int getKasillaKop(){
		return this.kasillakop;
	}
	public void aldatuKasillaKop(int i){
		this.kasillakop=i;
		System.out.println(this.kasillakop);
	}
	public void aldatuEgoera(int j){
		this.egoera=j;
	}
	public int getEgoera(){
		return this.egoera;
	}
	public boolean tableroanDago(int x,int y){
		boolean dago=true;
		if(x<0||x>9){
			dago=false;
		}else if(y<0|y>9){
			dago=false;
			
		}
		return dago;
	}
	public boolean konprobatuNorabidea(int x,int y){
		boolean posible=true;
		int i;
		if(this.egoera==0){
			if(x-(this.kasillakop-1)>=0){
				i=1;
				while(i<this.kasillakop && posible){
					if(!tableroa[x-i][y].isEnabled())
						posible=false;
					i++;
				}
			}else{
				posible=false;
			}
		}else if(this.egoera==1){
			if(x+(this.kasillakop-1)<=9){
				i=1;
				while(i<this.kasillakop && posible){
					if(!tableroa[x+i][y].isEnabled())
						posible=false;
					i++;
				}
			}else{
				posible=false;
			}
			
		}else if(this.egoera==2){
			if(y+(this.kasillakop-1)<=9){
				i=1;
				while(i<this.kasillakop && posible){
					if(!tableroa[x][y+i].isEnabled())
						posible=false;
					i++;
				}
			}else{
				posible=false;
			}
			
		}else{
			if(y-(this.kasillakop-1)>=0){
				i=1;
				while(i<this.kasillakop && posible){
					if(!tableroa[x][y-i].isEnabled())
						posible=false;
					i++;
				}
			}else{
				posible=false;
			}
			
		}
		return posible;
	}
	public void margotuOntziak(int x,int y){
		Ontzia ontzi;
		if(this.egoera==0){
			ontzi=this.ontziakSortu();
			Jokoa.getJokoa().jokalariaBueltatu().sartuListan(ontzi);
			for(int i=0;i<TableroaIG.getTableroaIG().getKasillaKop();i++){
				for(int j=x-i-1;j<x-i+2;j++){
					for(int k=y-1;k<y+2;k++){
						if(this.tableroanDago(j, k)){
							if(j==x-i && k==y){
								tableroa[j][k].setBackground(Color.BLACK);
								Jokoa.getJokoa().jokalariaBueltatu().sartuOntzia(j, k, ontzi);
							}else if(j==x-i-1 && k==y && i==this.kasillakop-1){
								tableroa[j][k].setEnabled(false);
							}else{
								tableroa[j][k].setEnabled(false);
							}
						}
					}
				}
				
			}
			
		}else if(this.egoera==1){
			ontzi=this.ontziakSortu();
			Jokoa.getJokoa().jokalariaBueltatu().sartuListan(ontzi);
			for(int i=0;i<TableroaIG.getTableroaIG().getKasillaKop();i++){
				for(int j=x+i-1;j<x+i+2;j++){
					for(int k=y-1;k<y+2;k++){
						if(this.tableroanDago(j, k)){
							if(j==x+i && k==y){
								tableroa[j][k].setBackground(Color.BLACK);
								Jokoa.getJokoa().jokalariaBueltatu().sartuOntzia(j, k, ontzi);
							}else if(j==x+i+1 && k==y && i==this.kasillakop-1){
								tableroa[j][k].setEnabled(false);
							}else{
								tableroa[j][k].setEnabled(false);
							}
						}
					}
				}
				
			}
			
		}else if(this.egoera==2){
			ontzi=this.ontziakSortu();
			Jokoa.getJokoa().jokalariaBueltatu().sartuListan(ontzi);
			for(int i=0;i<TableroaIG.getTableroaIG().getKasillaKop();i++){
				for(int j=x-1;j<x+2;j++){
					for(int k=y+i-1;k<y+i+2;k++){
						if(this.tableroanDago(j, k)){
							if(j==x && k==y+i){
								tableroa[j][k].setBackground(Color.BLACK);
								Jokoa.getJokoa().jokalariaBueltatu().sartuOntzia(j, k,ontzi);
							}else if(j==x && k==y+i+1 && i==this.kasillakop-1){
								tableroa[j][k].setEnabled(false);
							}else{
								tableroa[j][k].setEnabled(false);
							}
						}
					}
				}
				
			}
			
		}else{
			ontzi=this.ontziakSortu();
			Jokoa.getJokoa().jokalariaBueltatu().sartuListan(ontzi);
			for(int i=0;i<TableroaIG.getTableroaIG().getKasillaKop();i++){
				for(int j=x-1;j<x+2;j++){
					for(int k=y-i-1;k<y-i+2;k++){
						if(this.tableroanDago(j, k)){
							if(j==x && k==y-i){
								tableroa[j][k].setBackground(Color.BLACK);
								Jokoa.getJokoa().jokalariaBueltatu().sartuOntzia(j, k,ontzi);
							}else if(j==x && k==y-i-1 && i==this.kasillakop-1){
								tableroa[j][k].setEnabled(false);
							}else{
								tableroa[j][k].setEnabled(false);
							}
						}
					}
				}
			}
		}
		if (this.kasillakop==4){
			this.kenduHegazkin();
			hegazkina.setText("Hegazkina x"+TableroaIG.nireTableroaIG.getHegazkin());
			if(TableroaIG.nireTableroaIG.getHegazkin()==0){
				hegazkina.setEnabled(false);
			}
		}
		else if(this.kasillakop==3){
			this.kenduItsaspeko();
			itsaspeko.setText("Itsaspeko x"+TableroaIG.nireTableroaIG.getItsaspeko());
			if(TableroaIG.nireTableroaIG.getItsaspeko()==0){
				itsaspeko.setEnabled(false);
			}
		}
		else if(this.kasillakop==2){
			this.kenduSuntsitzaile();
			suntsitzailea.setText("Suntsitzailea x"+TableroaIG.nireTableroaIG.getSuntsitzaile());
			if(TableroaIG.nireTableroaIG.getSuntsitzaile()==0){
				suntsitzailea.setEnabled(false);
			}
			
		}
		else if(this.kasillakop==1){
			this.kenduFragata();
			fragata.setText("Fragata x"+TableroaIG.nireTableroaIG.getFragata());
			if(TableroaIG.nireTableroaIG.getFragata()==0){
				fragata.setEnabled(false);
			}
		}
		this.kasillakop=0;
		if(this.fragatKop==0&&this.hegazkinKop==0&&this.suntsiKop==0&&this.itsasKop==0){
			this.kasillakAktibatu();
			this.armakJarri();
		}
		
	}
    private void kasillakAktibatu(){
    	int i,j;
    	for (i=0;i<=9;i++){
    		for(j=0;j<=9;j++){
    			tableroa[i][j].setEnabled(true);
    		}
    	}
    }
    private void armakJarri(){
    	JButton bomba = new JButton("Bomba ");
		zureArmak.add(bomba);
		bomba.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
			}
			});
		JButton misila = new JButton("Misila x 1");
		zureArmak.add(misila);
		bomba.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
			}
			});
		JButton ezkutua = new JButton("Ezkutua x 1");
		zureArmak.add(ezkutua);
		bomba.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
			}
			});
		JButton misilIH = new JButton("Misil zuzendu IH x 0");
		zureArmak.add(misilIH);
		bomba.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
			}
			});
		JButton misilME = new JButton("Misil zuzendu ME x 0");
		zureArmak.add(misilME);
		bomba.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
			}
			});
		JButton misilBOOM = new JButton("Misil zuzendu Boom x 0");
		zureArmak.add(misilBOOM);
		bomba.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
			}
			});
		JButton radar = new JButton("Radar x 4");
		zureArmak.add(radar);
		bomba.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
			}
			});
    }
    public Ontzia ontziakSortu(){
    	Ontzia ontzi=null;
    	if(this.kasillakop==4){
    		ontzi=new HegazkinOntzi();
    		
    	}else if(this.kasillakop==3){
    		ontzi=new ItsaspekoOntzi();
    	}else if(this.kasillakop==2){
    		ontzi=new SuntsitzaileOntzi();
    	}else{
    		ontzi=new FragataOntzi();
    	}
    	return ontzi;
    }
}
